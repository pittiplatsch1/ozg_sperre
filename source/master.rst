

Dokumentation des Fachverfahrens "Melderegistersperre"
======================================================

:Autoren: Christoph Wolf, Ole Schwarz, Alexander Schröder, Jannik Walter

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Inhalt:

   ./inhalt/dokumentation.rst
   ./inhalt/git.rst
   ./inhalt/Pflichtenheft.rst
   

Suche und Stichwortverzeichnis
------------------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
