Einrichtung von Git
===================

.. contents:: Übersicht


Git verwenden (Upload und Download)
-----------------------------------

Ein vorhandener Git-Account wird hierfür vorausgesetzt. 

Folgende Schritte müssen nun ausgeführt werden:

.. code-block:: bash
    :linenos:

    $ Git global setup
    $ git config --global user.name "Vorname Nachname"
    $ git config --global user.email "mail@adresse.de"

Create a new repository

.. code-block:: bash
    :linenos:

    $ git clone git@gitlab.com:NUTZERNAME/PROJEKTNAME.git
    $ cd PROJEKTNAME
    $ touch README.md
    $ git add README.md
    $ git commit -m "add README"
    $ git push -u origin master

Push an existing folder

.. code-block:: bash
    :linenos:

    $ cd existing_folder
    $ git init
    $ git remote add origin git@gitlab.com:pittiplatsch1/ozg_sperre.git
    $ git add .
    $ git commit -m "Initial commit"
    $ git push -u origin master

Push an existing Git repository

.. code-block:: bash
    :linenos:

    $ cd existing_repo
    $ git remote rename origin old-origin
    $ git remote add origin git@gitlab.com:pittiplatsch1/ozg_sperre.git
    $ git push -u origin --all
    $ git push -u origin --tags


