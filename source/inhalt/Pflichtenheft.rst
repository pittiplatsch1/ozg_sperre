Pflichtenheft
=============

** Einleitung
** Allgemeines
*** Ziel und Zweck dieses Dokuments
    Dieses Pflichtenheft beschreibt einen Internetdienst in Form einer Webseite.  Diese Seite bezieht sich auf das Verfahren der Melderegistersperrung.
*** Projektbezug
    Das Projekt bezieht sich auf alle Personen, die sich für dieses Verfahren interessieren.
*** Ablage, Gültigkeit und Bezüge zu anderen Dokumenten
    Die Webseite ist aufrufbar im OZG-Portal. Es ist spätestens ab 2022 gültig.
** Verteiler und Freigabe
*** Verteiler für dieses Lastenheft

    |-------+------+---------+--------+-------------+--------------------------------------------------------------------------|
    | Rolle              |  Name                  | Telefon | E-Mail                             | Bemerkungen | Projektleiter |
    |-------+------+---------+--------+-------------+--------------------------------------------------------------------------|
    | Pflichtenheft      |  Alexander Schröder    |         | alexander.schroeder@th-wildau.de   |             | Peter Koppatz |
    |-------+------+---------+--------+-------------+--------------------------------------------------------------------------|
    |-------+------+---------+--------+-------------+--------------------------------------------------------------------------|
    | Formular           |  Christoph Wolf        |         | christoph.wolf@th-wildau.de        |             | Peter Koppatz |
    |-------+------+---------+--------+-------------+--------------------------------------------------------------------------|
    |-------+------+---------+--------+-------------+--------------------------------------------------------------------------|
    | Dokumentation      |  Ole Schwarz           |         | ole.schwarz@th-wildau.de           |             | Peter Koppatz |
    |-------+------+---------+--------+-------------+--------------------------------------------------------------------------|
    |-------+------+---------+--------+-------------+--------------------------------------------------------------------------|
    | Layout             |  Jannik Walter         |         | jannik.walter@th-wildau.de         |             | Peter Koppatz |
    |-------+------+---------+--------+-------------+--------------------------------------------------------------------------|

    Ihr Text
** Reviewvermerke und Meeting-Protokolle
*** Erstes bis n-tes Review
    15.11.2019: Die Rollenverteilung wurde geklärt.
    22.11.2019: Die Dokumentation wurde fertiggestellt.
    29.11.2019: Das Formular wurde der Webseite hinzugefügt.
    13.12.2019: Die XML-Datei wurde erstellt.
    20.12.2019: Die Dokumentation wurde fertiggestellt.
    13.01.2020: Das Pflichtenheft wurde fertiggestellt.
* Konzept und Rahmenbedingungen

** Benutzer / Zielgruppe
    Die Zielgruppe besteht aus Leuten, welche sich für dieses Verfahren interessieren. Diese Plattform ist für Einzelpersonen gedacht. Es werden Basiskenntnisse in der Internetnutzung vorausgesetzt.
** Ziele des Anbieters
   Das Ziel des Anbieters ist es dem Nutzer einen Verwaltungsdienst im Internet frei zugänglich zu machen.
** Ziele und Nutzen des Anwenders
   Die Anwender haben den Vorteil, dass sie sich nun im Internet über diese Verfahren informieren können sowie dieses Formular zuhause ausfüllen und ausdrucken können. Das soll Anwender entlasten, da diese nicht mehr zu den Verwaltungen kommen müssen, um diese Formulare zu beantragen.
** Systemvoraussetzungen
   Es wird lediglich vorausgesetzt einen Internetzugang zu haben, um die Seite aufrufen zu können.

** Übersicht der Meilensteine
*** Vorbereitungsphase
    Schritt 1: Es wurden die Rollen und die dazugehörigen Aufgaben verteilt.

    Schritt 2: Es wurden interne Fristen festgelegt.

    Schritt 3: Die Aufgaben wurden zu den festgelegten Fristen fertiggestellt.

    Schritt 4: Die Seite wurde implementiert und getestet.

*** Implementierung und Test
    Schritt 1: Es wird überprüft, ob alle nötigen Inhalte vorhanden sind und ob alle nötigen Funktionen nutzbar sind.

    Schritt 2: Alle Seiten und Formulare werden in das OZG-Portal eingebunden.

    Schritt 3: Es wird überprüft, ob alle Seiten aufrufbar sind.


** Voraussichtlicher Verkaufsstart
   Anfang 2022


** Testhinweise
   Es muss unbedingt darauf geachtet werden, dass alle Seiten aufrufbar ist und das Formular online ausfüllbar ist. Es dürfen beim Test keine Fehler auftreten.
** Vergleich mit bestehenden Lösungen
   Alle bestehenden Lösungen sind nicht online öffentlich zugänglich. Durch das Abholen der Formulare in den Verwaltugen besteht für die Nutzer ein hoher zeitlicher Aufwand.
** Schätzung des Aufwands
   Der Aufwand ist im Vergleich zu den alten bestehenden Verfahren relativ gering.


  Datum: 14.01.2020

  Unterschrift Auftraggeber:
 
  Unterschrift Projektleiter:
 
  Weitere Unterschriften:
 
