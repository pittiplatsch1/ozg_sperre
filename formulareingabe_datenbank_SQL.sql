SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `melderegistersperre` DEFAULT CHARACTER SET latin1 COLLATE latin1_german2_ci;
USE `melderegistersperre`;

CREATE TABLE `tbl_formulardaten` (
`id` mediumint(9) NOT NULL,
`Anrede` varchar(20) COLLATE utf8_german2_ci NOT NULL,
`Vorname` varchar(100) COLLATE utf8_german2_ci NOT NULL,
`Nachname` varchar(120) COLLATE utf8_german2_ci NOT NULL,
`Geburtsdatum` varchar(10) COLLATE utf8_german2_ci NOT NULL,
`Strasse` varchar(150) COLLATE utf8_german2_ci NOT NULL,
`PLZ` int(11) NOT NULL,
`Stadt` varchar(100) COLLATE utf8_german2_ci NOT NULL,
`SperreArt` varchar(50) COLLATE utf8_german2_ci NOT NULL,
`EMail` varchar(120) COLLATE utf8_german2_ci NOT NULL,
`Begruendung` varchar(5000) COLLATE utf8_german2_ci NOT NULL,
`Uhrzeit` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

ALTER TABLE `tbl_formulardaten`
ADD PRIMARY KEY (`id`);

ALTER TABLE `tbl_formulardaten`
MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;