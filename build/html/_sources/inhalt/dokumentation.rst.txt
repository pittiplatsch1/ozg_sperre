Einrichtung der Softwarekomponenten
===================================

.. contents:: Übersicht

**Der Microservice läuft zum Test auf einem eingerichtetem Server**
-------------------------------------------------------------------
Die Serveradresse lautet:  http://wp13440588.server-he.de 

1. Übersicht
------------

Wir haben uns dazu entschieden, das Projekt mit einer LLAMP-Server-Kombination aus einem Apache-Webserver und einer MySQL-Datenbank zu betreiben.
Der Microservice selbst besteht aus HTML-Seiten, mit angebundenen PHP-Skripten. Letztenendes handelt es sich dadurch bei den Seiten des Microservices um .php-Dateien.
Die Dokumentation dieses Services, wird durch Spinx ersellt (diese Seite).
Die Installation aller beschriebenen Bestandteile wird lediglich unter Windows ausgeführt. Linux-Befehle werden hier nicht berücksichtigt.


2. Beschreibung Melderegistersperre
-----------------------------------

Grundsätzlich kann jedermann über eine bestimmte Person auf Antrag eine Melderegisterauskunft erhalten. Sie können jedoch eine Auskunftssperre ins Melderegister eintragen lassen, wenn Ihnen als Betroffenen oder einer anderen Person durch die Bekanntgabe Ihrer Anschrift eine Gefahr für Leben, Gesundheit, persönliche Freiheit oder ähnliche schutzwürdige Interessen entstehen könnte.

Die Auskunftssperre hat nur Auswirkungen auf den privaten Bereich (Privatpersonen, Firmen, Rechtsanwälte u. ä.).

Behörden und sonstige öffentliche Stellen erhalten weiterhin Auskunft.

Die Auskunftssperre ist auf zwei Jahre befristet. Sie kann auf erneuten Antrag verlängert werden.

3. Rechtliche Grundlage
-----------------------

Als rechtliche Grundlage für diese Fachanwendung dient der § 51 Bundesmeldegesetz (BMG).


4. XAMPP installieren 
---------------------
XAMPP ist ein Software-Paket welches verschiedene LLAMP-Serverbestandteile beinhaltet. Unteranderem Apache Webserver und MySQL.
Diese beiden Bestandteile dienen für unser Projekt als Testgrundlage und bringen die PHP-Skripte zum laufen.
Im offiziellen Betrieb ist ein Webserver und eine Datenbank notwendig, um den Service ordnungsgemäß zu betreiben. 

XAMPP downloaden:
.. https://www.apachefriends.org/de/download.html

Nach Installation ist im Installationsverzeichnis eine Datei "xampp-control.exe". Wird diese ausgeführt, so erhält der Benutzer eine grafische Oberfläche,
über welche er den Apache-Webserver und MySQL über den Start-Button starten kann. 
Der Server und die Datenbank laufen nun und sind unter "localhost/dashboard" bzw. die Datenbank unter "localhost/phpmyadmin" über einen beliebigen Browser erreichbar.

5. Bootstrap Installation und Verwendung
----------------------------------------

Bootstrap downloaden:
.. https://getbootstrap.com/docs/4.1/getting-started/download/

Es ist zu empfehlen, die compiled JS und CSS-Version herunterzuladen. Der Benutzer erhält eine .zip-Datei, welche entpackt werden muss. 
Der entpackte Ordner ist für die Verwendung mit dem Webserver im Installationsverzeichnis von XAMPP unter "../XAMPP/htdocs" abzulegen.
Von hieraus kann die erste index-Datei über "localhost/webseitenname/index" erreicht werden.

Es obliegt dem Benutzer ob dieser sich Bootstrap-Vorlagen herunterlädt und in den Bootstrap-Ordner unter htdocs einfügt, oder die Seiten durch die von Bootstrap mitgelieferten
Elemente selbst zusammen baut.
Wir entschieden uns für letzteres.

Wenn der von uns entworfene Service verwendet werden soll, so ist der Ordner "melderegistersperre" von Gitlab herunterzuladen und auf einen Webserver oder wie oben beschrieben einzufügen.
.. https://gitlab.com/pittiplatsch1/ozg_sperre 

5.1 Systemvoraussetzungen
^^^^^^^^^^^^^^^^^^^^^^^^^
Die Systemvoraussetzungen für unseren Service werden hier aufgeführt:

   beliebiges Betriebssystem mit XAMPP (erhältlich für Windwos, Linux und MacOS)
   Eine gestartete Datenbank (MySQL) und ein Webserver (Apache)

6. Erklärung der Seiten
-----------------------

Im Zuge des Aufbaus unseres Microservices entstanden folgende Seiten bzw. Dateien:

**index.php**
Die Basis unseres Webservices. Von hier kann der Benutzer in alle Bereiche des Services navigieren und erhält zudem Informationen zum Thema.

**hinweise.php**
Hier erhält der Nutzer hinweise über das Thema der Melderegistersperre. Diese Seite dient zur reinen Information.

**hilfe.php**
Hier erhält der Nutzer Informationen über Anlaufstellen, an denen er Hilfe bekommt, wenn Probleme mit dem Service oder dem Verständnis auftreten. Diese Seite dient zur reinen Information.

**impressum.php**
Hier erhält der Nutzer nach §5 des TMG Informationen über den Herausgeber und Verantwortlichen dieser Seite. Sie ist in unserem Test-Servive leer, da kein offizieller Herausgeber zur Verfügung steht. 

**antrag.php**
Dies ist das Kernelement des eigentliches Services zur Melderegistersperre. Hier kann der Nutzer über das Formular notwendige Daten eingeben. Eine Validierung von Daten erfolgt derzeit nur im Feld "E-Mail".
Über den Abschicken-Button am Ende des Formulars können die Daten übermittelt werden. Nach Übermittlung erfolgt eine Weiterleitung zur Hauptseite (index.php).
Die Übermittlung wird über Folgendes PHP-Skript getätigt, welches am HTML-Teil der Website anschließt:

.. code-block:: php

    <?php
   if(isset($_POST['abschicken']))
   {
   require("inc/db_connect.php");

    $anrede = $_POST['anrede'];
    $vorname = $_POST['vorname'];
    $nachname = $_POST['nachname'];
    $straßeundhausnr = $_POST['strasseundhausnr'];
    $plz = $_POST['plz'];
    $stadt = $_POST['stadt'];
    $email = $_POST['email'];
    $sperreart = $_POST['sperreart'];
    $begruendung = $_POST['begruendung'];
    $datum = date("Y-m-d H:i:s");

    $sql = "INSERT INTO tbl_formulardaten(Anrede, Vorname, Nachname, Strasse, PLZ, Stadt, EMail, SperreArt, Begruendung, Uhrzeit) VALUES (:anrede, :vorname, :nachname, :strasse, :plz, :stadt, :email, :sperre, :begruendung, :uhrzeit)";
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue(':anrede', $anrede);
    $stmt->bindValue(':vorname', $vorname);
    $stmt->bindValue(':nachname', $nachname);
    $stmt->bindValue(':strasse', $straßeundhausnr);
    $stmt->bindValue(':plz', $plz);
    $stmt->bindValue(':stadt', $stadt);
    $stmt->bindValue(':email', $email);
    $stmt->bindValue(':sperre', $sperreart);
    $stmt->bindValue(':begruendung', $begruendung);
    $stmt->bindValue(':uhrzeit', $datum);

    $stmt->execute();
   }
   ?>

Dieses PHP-Skript überprüft im ersten Schritt, ob der Button im Formular mit dem Namen "abschicken" betätigt wurde. Ist dies der Fall, startet die Schleife.
Um weitere Schritte auszuführen, ist eine Datei unter "inc/db_connect.php" required. Diese Datei wird im Folgenden erklärt. Sie stellt die Verbindung zur Datenbank her, in welche die Eingaben aus dem Formular eingetragen werden sollen.
Anschließend bekommen alle Formularfelder, welche über die _POST-Methode Ihre Daten übermittelt, eine eigene Variable z.B. $anrede. Dies wird für alle Formularfelder im oberen Block des PHP-Skripts getan.
Im Zweiten Block wird nach der $sql-Variable der SQL-Befehl für die Datenbank vermerkt und an die db_connect.php-Datei weitergeben, welche dies Vorbereitet.
Nachfolgend werden die Inhalte der Variablen z.B. $anrede an die Datenbankspalte :anrede gebunden. Dies geschieht im Rahmen einer neuen Variable.

Anschließend werden in der letzten Zeile, alle Befehle ausgeführt.

**/inc/db_connect.php**

Diese Datei hat folgenden Inhalt:

.. code-block:: php

   <?PHP

   $user = "NUTZERNAME";
   $pass = "PASSWORT";

   try
   {
   $dbh = new PDO('mysql:host=localhost;dbname=melderegistersperre;charset=utf8', $user, $pass);
   $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   }
   catch (PDOException $e)
   {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
   }
   ?>

In dieser werden im ersten Schritt Variablen für den Login eines Benutzers auf der Datenbank festgelegt. Diese db_connect-Datei ist über den Server als einzelne nicht aufrufbar und auch nicht auslesbar. Somit bleiben deren Inhalte geheim.
Im try-Block versucht das Skript eine Verbindung zu angebeben Datenbank herzustellen. Sollte dies nicht gelingen, wird der Fehler über catch-Anweisungen abgefangen. 
Im unteren Abschnitt dieser Dokumentation wird ein SQL-Skript eingefügt, welches Sie in Ihre Datenbank importieren bzw. eingeben können, damit die korrekten Datenbanktabellen etc. automatisch erstellt werden. Die Benutzer, Datenbanken und Tabellen müssen natürlich mit dieser db_connect-Datei korrelieren.

**login.php**

Diese Seite/Datei ist für Sachbearbeiter vorgesehen. Diese können sich nur mit Einlogg-Daten anmelden. Daten liegen hierfür in einer seperaten Datenbank bereit. Diese Datenbank enthält eine Tabelle mit Informationen über Benutzername, Passwort und Admin-Status. Wie diese Datenbank erstellt wird, erfahren Sie im Folgenden. 
Das Konto für die Erstverwendung lautet:

 Name: admin
 Passwort: phpadmin

 Diese Daten müssen in der Login-Datenbank umgehend geändert werden. 

Anschließend lassen sich im Dashboard, welches sich nach erfolgreichem Login öffnet, neue Benutzer im Reiter links anlegen. Das Passwort wird im Eingabefeld im Klartext eingegeben und der erzeugte Hashwert an den Datenbank-Adminsitrator übermittelt. 
Dieser legt einen neuen Benutzer an, in dem einen gewählten Nutzernamen und den übermittelten Hash-Wert in die Datenbank-Login-Tabelle einfügt.

**/login/dashboard.php**
Das Dashboard ist nur nach erfolgreichem Login über die login.php aufrufbar. Hier werden Eingaben aus dem Melderegistersperre-Formular angezeigt und können vom Sachbearbeiter bearbeitet werden.
Wie die dashboard.php-Datei mit Ihrer Datenbank auf Ihrem Server verknüpft wird, wird weiter unten beschrieben.
Nach Logout aus dem Dashboard werden sie zur index.php der Melderegistersperre-Website weitergeleitet und haben keinen Zugriff mehr auf das Dashboard.

6.1 Die Melderegistersperre-Formular-Datenbank verknüpfen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die Daten welche über das Formular auf der Website eingegeben werden, werden an eine Datenbank weitergeleitet. Diese Datenbank muss erstellt werden, mit folgendem Befehl, welcher in Ihre Datenbank-Admin-Oberfläche eingebeben werden muss oder:
Eine formulareingabe_datenbank_SQL.sql-Datei ist im Repository vorhanden und muss nur in die PhpMyAdmin-Oberfläche über den Reiter "Importieren" importiert werden. Die Datenbank, sämtliche Tabellen usw. werden somit automatisch erstellt.

.. code-block:: sql

   SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
   SET AUTOCOMMIT = 0;
   START TRANSACTION;
   SET time_zone = "+00:00";

   CREATE DATABASE IF NOT EXISTS `melderegistersperre` DEFAULT CHARACTER SET latin1 COLLATE latin1_german2_ci;
   USE `melderegistersperre`;

   CREATE TABLE `tbl_formulardaten` (
   `id` mediumint(9) NOT NULL,
   `Anrede` varchar(20) COLLATE utf8_german2_ci NOT NULL,
   `Vorname` varchar(100) COLLATE utf8_german2_ci NOT NULL,
   `Nachname` varchar(120) COLLATE utf8_german2_ci NOT NULL,
   `Strasse` varchar(150) COLLATE utf8_german2_ci NOT NULL,
   `PLZ` int(11) NOT NULL,
   `Stadt` varchar(100) COLLATE utf8_german2_ci NOT NULL,
   `SperreArt` varchar(50) COLLATE utf8_german2_ci NOT NULL,
   `EMail` varchar(120) COLLATE utf8_german2_ci NOT NULL,
   `Begruendung` varchar(5000) COLLATE utf8_german2_ci NOT NULL,
   `Uhrzeit` datetime NOT NULL
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

   ALTER TABLE `tbl_formulardaten`
   ADD PRIMARY KEY (`id`);

   ALTER TABLE `tbl_formulardaten`
   MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
   COMMIT;

Die Datenbank ist nun erstellt. Sie müssen nun noch einen Benutzer auf dieser Datenbank anlegen. Dessen Nutzername und Passwort muss in die db_connect.php-Datei in den ersten beiden Variablen $user und $pass jeweils eingetragen werden.
Die Datenbank ist nun verknüpft.

6.2 Die Login-Datenbank erstellen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Um einen Login in das Dashboard zu ermöglichen, muss Folgende Datenbank wie oben genannt erstellt werden:
Eine login_datenbank_SQL.sql-Datei ist im Repository vorhanden und muss nur in die PhpMyAdmin-Oberfläche über den Reiter "Importieren" importiert werden. Die Datenbank, sämtliche Tabellen usw. werden somit automatisch erstellt.

.. code-block:: sql

   SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
   SET AUTOCOMMIT = 0;
   START TRANSACTION;
   SET time_zone = "+00:00";

   CREATE DATABASE IF NOT EXISTS `secure_login` DEFAULT CHARACTER SET latin1 COLLATE latin1_german2_ci;
   USE `secure_login`;

   CREATE TABLE `tbl_users` (
   `username` varchar(50) NOT NULL,
   `password` varchar(255) NOT NULL,
   `admin` tinyint(1) NOT NULL DEFAULT 0
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


   INSERT INTO `tbl_users` (`username`, `password`, `admin`) VALUES
   ('admin', '$2y$10$A/uCY4Q.B/zEB.Za68hw6Ol9E86wLjP1b2WqOJR6.hRh3h2HCaeB2', 1),

   ALTER TABLE `tbl_users`
   ADD PRIMARY KEY (`username`);
   COMMIT;
   

Die Datenbank wurde nun erstellt und ein Login durch den Erstbenutzer ist möglich.
Name: admin
Passwort: phpadmin
Der Account sollte umgehend aus der Datenbank entfernt werden,  nachdem ein neuer Benutzer mit anderem Passwort (Hashwert) angelegt wurde.

6.3 Das Dashboard mit der Melderegister-Formular-Datenbank verbinden
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Auf der Login-Datenbank muss ein Nutzer erstellt sein, welcher den Namen "root" und das Passwort "phpadmin" trägt. Ist dies gegeben, ist die Datenbank nun verknüpft.
Soll Benutzername und Passwort geändert werden, geschieht dies in der Datei unter /login-system/inc/db.php 
Die zuändernden Stellen sind eindeutig zu erkennen.

7. Akteure
----------

Beteiligte Akteure und Ihre Use Cases sind im folgenden UML-Diagramm dargestellt.

.. figure:: /pics/umldiagramm.png
    :name: UML-Diagramm
    :alt:  UML-Diagramm
    :align: center
    :width: 100%

    UML-Diagramm der Akteure und Use-Cases

8. Daten als XML exportieren
----------------------------
Der Export der Eingebenen Formulardaten erfolgt in unserem Service lediglich über die Datenbankverwaltung z.B. phpMyAdmin. Hierfür wird die zu exportierende Datenbank ausgewählt und in der oberen Leiste von phpMyAdmin auf "Exportieren" geklickt.
Im Unteren Bereich der Export-Seite wird Format "XML" ausgewählt und mit "Ok" bestätigt. 
Ein Export der Daten als XML steht nun zum Download bereit, welcher automatisch starten sollte.
Eine weitere Exportmöglichkeit ist derzeit nicht vorhanden, wurde in der Aufgabenstellung auch nicht näher definiert.

9. Anleitung für Bürger
-----------------------
Die Anleitung für Bürger hält sich aufgrund der Einfachheit des Services relativ überschaubar.
Über den Button "Antrag stellen" oben rechts, rot hervorgehoben, kann der Bürger seinen Antrag stellen.
Auf der Antragseite findet er ein Formular vor, in welches er seine Daten einträgt. Die Daten werden validiert und auf Sinnhaftigkeit überprüft.
Über den Button "Absenden" wird der Antrag an die Behörde weitergeleitet.
Der Vorgang ist an dieser Stelle für den Bürger beendet.

10. Pyhton installieren
-----------------------

Python installieren:
.. https://www.python.org/downloads/

Ein für die Installation von Sphinx notwendiger Zustand ist die Erstellung und Aktivierung eines Environments:

.. code-block:: bash
   
   py -3 -m venv env 

Gleiches wird über

.. code-block:: bash 

   .\env\Scripts\activate.bat 

aktviert. 

11. Sphinx installieren
-----------------------

.. code-block:: bash

    $ pip install sphinx

Dokumentation erstellen:

.. code-block:: bash
    :linenos:

    $ mkdir dokumentation
    $ cd dokumentation
    $ sphinx-quickstart

HTML erzeugen lassen:

.. code-block:: bash

    $ make html

