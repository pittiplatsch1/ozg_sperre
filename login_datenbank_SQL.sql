-- phpMyAdmin SQL Dump
-- version 4.9.2deb2
-- https://www.phpmyadmin.net/
--
-- Host: wp569.webpack.hosteurope.de
-- Erstellungszeit: 07. Jan 2020 um 11:37
-- Server-Version: 5.6.43-84.3-log
-- PHP-Version: 7.2.24-1+0~20191026.31+debian9~1.gbpbbacde

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `db13440588-login`
--
CREATE DATABASE IF NOT EXISTS `secure_login` DEFAULT CHARACTER SET latin1 COLLATE latin1_german2_ci;
USE `secure_login`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_users`
--

CREATE TABLE `tbl_users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `tbl_users`
--

INSERT INTO `tbl_users` (`username`, `password`, `admin`) VALUES
('chris.wolf', '$2y$10$pOBtGgYGlyd/fNoQIul9pe66CUjuZyi0wB.sz/XxXFbDhQjiWzPIu', 0),
('Formularbearbeiter', '$2y$10$4rkscR3qseovSoh.hilfL.mao4WI6eNzQvdPkkIbya4CIFedSIdPK', 1),
('ole.schwarz', '$2y$10$jvtoCK9R4XXH2r5j/cbf5.Y/BEoW9fruCcwW8.VwUfma5/OE3Y1xm', 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
