<!doctype html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <title>Melderegistersperre</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A corporate Bootstrap theme by Medium Rare">
    <link href="assets/css/loaders/loader-typing.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/css/theme-desktop-app.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="preload" as="font" href="assets/fonts/Inter-UI-upright.var.woff2" type="font/woff2"
        crossorigin="anonymous">
    <link rel="preload" as="font" href="assets/fonts/Inter-UI.var.woff2" type="font/woff2" crossorigin="anonymous">
</head>

<body>
    <div class="loader">
        <div class="loading-animation"></div>
    </div>

    <div class="navbar-container bg-primary-3">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary-3" data-sticky="top">
            <div class="container">
                <a class="navbar-brand" href="index.php">Schleswig-Holstein</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <img class="icon navbar-toggler-open" src="assets/img/icons/interface/menu.svg"
                        alt="menu interface icon" data-inject-svg />
                    <img class="icon navbar-toggler-close" src="assets/img/icons/interface/cross.svg"
                        alt="cross interface icon" data-inject-svg />
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <div class="py-2 py-lg-0">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a href="hilfe.php" class="nav-link dropdown-toggle" data-toggle="dropdown-grid"
                                    aria-expanded="false" aria-haspopup="true">Hilfe</a>
                            <li class="nav-item dropdown">
                                <a href="impressum.php" class="nav-link dropdown-toggle" data-toggle="dropdown-grid"
                                    aria-expanded="false" aria-haspopup="true">Impressum</a>
                            <li class="nav-item dropdown">
                            <li class="nav-item dropdown">
                                <a href="login-system/login.php" class="nav-link dropdown-toggle"
                                    data-toggle="dropdown-grid" aria-expanded="false" aria-haspopup="true">Login</a>
                            <li class="nav-item dropdown">
                                <a href="antrag.php" class="btn btn-primary ml-lg-3">Antrag stellen</a>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <section class="bg-primary-3 text-light text-center has-divider header-desktop-app">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8 col-md-9">
                    <h1 class="display-3">Melderegister-Sperre des Landes Schleswig-Holstein</h1>

                    <div class="d-flex flex-column flex-sm-row justify-content-center mt-4">
                        <a href="hinweise.php" class="btn btn-lg btn-primary mx-2 mb-2 mb-sm-0">Hinweise</a>

                    </div>
                </div>
            </div>
            <div class="row justify-content-center mt-6" data-aos="fade-up" data-delay="100">
                <div class="col-lg-10">
                    <img src="assets/img/schleswig-holstein3.jpg" alt="Image" class="rounded shadow-lg">
                </div>
            </div>
        </div>
        <div class="divider">
            <img src="assets/img/dividers/divider-2.svg" alt="graphical divider" data-inject-svg />
        </div>
    </section>


    <section>
        <div class="container">
            
                <div class="col">
                    <h2 class="display-4">Dokumentation</h2>
                    <h1><a class="toc-backref" href="#id2">Einrichtung der Softwarekomponenten</a><a class="headerlink" href="#einrichtung-der-softwarekomponenten" title="Link zu dieser Überschrift">¶</a></h1>
<div class="contents topic" id="ubersicht">
<p class="topic-title first">Übersicht</p>
<ul class="simple">
<p><a class="reference internal" href="#einrichtung-der-softwarekomponenten" id="id2">Einrichtung der Softwarekomponenten</a></p>
<ul>
<p><a class="reference internal" href="#der-microservice-lauft-zum-test-auf-einem-eingerichtetem-server" id="id3"><strong>Der Microservice läuft zum Test auf einem eingerichtetem Server</strong></a></p></li>
<p><a class="reference internal" href="#id1" id="id4">1. Übersicht</a></p></li>
<p><a class="reference internal" href="#beschreibung-melderegistersperre" id="id5">2. Beschreibung Melderegistersperre</a></p></li>
<p><a class="reference internal" href="#rechtliche-grundlage" id="id6">3. Rechtliche Grundlage</a></p></li>
<p><a class="reference internal" href="#xampp-installieren" id="id7">4. XAMPP installieren</a></p></li>
<p><a class="reference internal" href="#bootstrap-installation-und-verwendung" id="id8">5. Bootstrap Installation und Verwendung</a></p>
<ul>
<p><a class="reference internal" href="#systemvoraussetzungen" id="id9">5.1 Systemvoraussetzungen</a></p></li>
</ul>

<p><a class="reference internal" href="#erklarung-der-seiten" id="id10">6. Erklärung der Seiten</a></p>
<ul>
<p><a class="reference internal" href="#die-melderegistersperre-formular-datenbank-verknupfen" id="id11">6.1 Die Melderegistersperre-Formular-Datenbank verknüpfen</a></p></li>
<p><a class="reference internal" href="#die-login-datenbank-erstellen" id="id12">6.2 Die Login-Datenbank erstellen</a></p></li>
<p><a class="reference internal" href="#das-dashboard-mit-der-melderegister-formular-datenbank-verbinden" id="id13">6.3 Das Dashboard mit der Melderegister-Formular-Datenbank verbinden</a></p></li>
</ul>

<p><a class="reference internal" href="#akteure" id="id14">7. Akteure</a></p></li>
<p><a class="reference internal" href="#daten-als-xml-exportieren" id="id15">8. Daten als XML exportieren</a></p></li>
<p><a class="reference internal" href="#anleitung-fur-burger" id="id16">9. Anleitung für Bürger</a></p></li>
<p><a class="reference internal" href="#pyhton-installieren" id="id17">10. Pyhton installieren</a></p></li>
<p><a class="reference internal" href="#sphinx-installieren" id="id18">11. Sphinx installieren</a></p></li>
</ul>

</ul>
</div>
<div class="section" id="der-microservice-lauft-zum-test-auf-einem-eingerichtetem-server">
<h2><a class="toc-backref" href="#id3"><strong>Der Microservice läuft zum Test auf einem eingerichtetem Server</strong></a><a class="headerlink" href="#der-microservice-lauft-zum-test-auf-einem-eingerichtetem-server" title="Link zu dieser Überschrift">¶</a></h2>
<p>Die Serveradresse lautet:  <a class="reference external" href="http://wp13440588.server-he.de">http://wp13440588.server-he.de</a></p>
</div>
<div class="section" id="id1">
<h2><a class="toc-backref" href="#id4">1. Übersicht</a><a class="headerlink" href="#id1" title="Link zu dieser Überschrift">¶</a></h2>
<p>Wir haben uns dazu entschieden, das Projekt mit einer LLAMP-Server-Kombination aus einem Apache-Webserver und einer MySQL-Datenbank zu betreiben.
Der Microservice selbst besteht aus HTML-Seiten, mit angebundenen PHP-Skripten. Letztenendes handelt es sich dadurch bei den Seiten des Microservices um .php-Dateien.
Die Dokumentation dieses Services, wird durch Spinx ersellt (diese Seite).
Die Installation aller beschriebenen Bestandteile wird lediglich unter Windows ausgeführt. Linux-Befehle werden hier nicht berücksichtigt.</p>
</div>
<div class="section" id="beschreibung-melderegistersperre">
<h2><a class="toc-backref" href="#id5">2. Beschreibung Melderegistersperre</a><a class="headerlink" href="#beschreibung-melderegistersperre" title="Link zu dieser Überschrift">¶</a></h2>
<p>Grundsätzlich kann jedermann über eine bestimmte Person auf Antrag eine Melderegisterauskunft erhalten. Sie können jedoch eine Auskunftssperre ins Melderegister eintragen lassen, wenn Ihnen als Betroffenen oder einer anderen Person durch die Bekanntgabe Ihrer Anschrift eine Gefahr für Leben, Gesundheit, persönliche Freiheit oder ähnliche schutzwürdige Interessen entstehen könnte.</p>
<p>Die Auskunftssperre hat nur Auswirkungen auf den privaten Bereich (Privatpersonen, Firmen, Rechtsanwälte u. ä.).</p>
<p>Behörden und sonstige öffentliche Stellen erhalten weiterhin Auskunft.</p>
<p>Die Auskunftssperre ist auf zwei Jahre befristet. Sie kann auf erneuten Antrag verlängert werden.</p>
</div>
<div class="section" id="rechtliche-grundlage">
<h2><a class="toc-backref" href="#id6">3. Rechtliche Grundlage</a><a class="headerlink" href="#rechtliche-grundlage" title="Link zu dieser Überschrift">¶</a></h2>
<p>Als rechtliche Grundlage für diese Fachanwendung dient der § 51 Bundesmeldegesetz (BMG).</p>
</div>
<div class="section" id="xampp-installieren">
<h2><a class="toc-backref" href="#id7">4. XAMPP installieren</a><a class="headerlink" href="#xampp-installieren" title="Link zu dieser Überschrift">¶</a></h2>
<p>XAMPP ist ein Software-Paket welches verschiedene LLAMP-Serverbestandteile beinhaltet. Unteranderem Apache Webserver und MySQL.
Diese beiden Bestandteile dienen für unser Projekt als Testgrundlage und bringen die PHP-Skripte zum laufen.
Im offiziellen Betrieb ist ein Webserver und eine Datenbank notwendig, um den Service ordnungsgemäß zu betreiben.</p>
<p>XAMPP downloaden:
.. <a class="reference external" href="https://www.apachefriends.org/de/download.html">https://www.apachefriends.org/de/download.html</a></p>
<p>Nach Installation ist im Installationsverzeichnis eine Datei „xampp-control.exe“. Wird diese ausgeführt, so erhält der Benutzer eine grafische Oberfläche,
über welche er den Apache-Webserver und MySQL über den Start-Button starten kann.
Der Server und die Datenbank laufen nun und sind unter „localhost/dashboard“ bzw. die Datenbank unter „localhost/phpmyadmin“ über einen beliebigen Browser erreichbar.</p>
</div>
<div class="section" id="bootstrap-installation-und-verwendung">
<h2><a class="toc-backref" href="#id8">5. Bootstrap Installation und Verwendung</a><a class="headerlink" href="#bootstrap-installation-und-verwendung" title="Link zu dieser Überschrift">¶</a></h2>
<p>Bootstrap downloaden:
.. <a class="reference external" href="https://getbootstrap.com/docs/4.1/getting-started/download/">https://getbootstrap.com/docs/4.1/getting-started/download/</a></p>
<p>Es ist zu empfehlen, die compiled JS und CSS-Version herunterzuladen. Der Benutzer erhält eine .zip-Datei, welche entpackt werden muss.
Der entpackte Ordner ist für die Verwendung mit dem Webserver im Installationsverzeichnis von XAMPP unter „../XAMPP/htdocs“ abzulegen.
Von hieraus kann die erste index-Datei über „localhost/webseitenname/index“ erreicht werden.</p>
<p>Es obliegt dem Benutzer ob dieser sich Bootstrap-Vorlagen herunterlädt und in den Bootstrap-Ordner unter htdocs einfügt, oder die Seiten durch die von Bootstrap mitgelieferten
Elemente selbst zusammen baut.
Wir entschieden uns für letzteres.</p>
<p>Wenn der von uns entworfene Service verwendet werden soll, so ist der Ordner „melderegistersperre“ von Gitlab herunterzuladen und auf einen Webserver oder wie oben beschrieben einzufügen.
.. <a class="reference external" href="https://gitlab.com/pittiplatsch1/ozg_sperre">https://gitlab.com/pittiplatsch1/ozg_sperre</a></p>
<div class="section" id="systemvoraussetzungen">
<h3><a class="toc-backref" href="#id9">5.1 Systemvoraussetzungen</a><a class="headerlink" href="#systemvoraussetzungen" title="Link zu dieser Überschrift">¶</a></h3>
<p>Die Systemvoraussetzungen für unseren Service werden hier aufgeführt:</p>
<blockquote>
<div><p>beliebiges Betriebssystem mit XAMPP (erhältlich für Windwos, Linux und MacOS)
Eine gestartete Datenbank (MySQL) und ein Webserver (Apache)</p>
</div></blockquote>
</div>
</div>
<div class="section" id="erklarung-der-seiten">
<h2><a class="toc-backref" href="#id10">6. Erklärung der Seiten</a><a class="headerlink" href="#erklarung-der-seiten" title="Link zu dieser Überschrift">¶</a></h2>
<p>Im Zuge des Aufbaus unseres Microservices entstanden folgende Seiten bzw. Dateien:</p>
<p><strong>index.php</strong>
Die Basis unseres Webservices. Von hier kann der Benutzer in alle Bereiche des Services navigieren und erhält zudem Informationen zum Thema.</p>
<p><strong>hinweise.php</strong>
Hier erhält der Nutzer hinweise über das Thema der Melderegistersperre. Diese Seite dient zur reinen Information.</p>
<p><strong>hilfe.php</strong>
Hier erhält der Nutzer Informationen über Anlaufstellen, an denen er Hilfe bekommt, wenn Probleme mit dem Service oder dem Verständnis auftreten. Diese Seite dient zur reinen Information.</p>
<p><strong>impressum.php</strong>
Hier erhält der Nutzer nach §5 des TMG Informationen über den Herausgeber und Verantwortlichen dieser Seite. Sie ist in unserem Test-Servive leer, da kein offizieller Herausgeber zur Verfügung steht.</p>
<p><strong>antrag.php</strong>
Dies ist das Kernelement des eigentliches Services zur Melderegistersperre. Hier kann der Nutzer über das Formular notwendige Daten eingeben. Eine Validierung von Daten erfolgt derzeit nur im Feld „E-Mail“.
Über den Abschicken-Button am Ende des Formulars können die Daten übermittelt werden. Nach Übermittlung erfolgt eine Weiterleitung zur Hauptseite (index.php).
Die Übermittlung wird über Folgendes PHP-Skript getätigt, welches am HTML-Teil der Website anschließt:</p>
<div class="highlight-php notranslate"><div class="highlight"><pre><span></span><span class="x"> </span><span class="cp">&lt;?php</span>
<span class="k">if</span><span class="p">(</span><span class="nb">isset</span><span class="p">(</span><span class="nv">$_POST</span><span class="p">[</span><span class="s1">&#39;abschicken&#39;</span><span class="p">]))</span>
<span class="p">{</span>
<span class="k">require</span><span class="p">(</span><span class="s2">&quot;inc/db_connect.php&quot;</span><span class="p">);</span>

 <span class="nv">$anrede</span> <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">&#39;anrede&#39;</span><span class="p">];</span>
 <span class="nv">$vorname</span> <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">&#39;vorname&#39;</span><span class="p">];</span>
 <span class="nv">$nachname</span> <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">&#39;nachname&#39;</span><span class="p">];</span>
 <span class="nv">$straßeundhausnr</span> <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">&#39;strasseundhausnr&#39;</span><span class="p">];</span>
 <span class="nv">$plz</span> <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">&#39;plz&#39;</span><span class="p">];</span>
 <span class="nv">$stadt</span> <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">&#39;stadt&#39;</span><span class="p">];</span>
 <span class="nv">$email</span> <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">&#39;email&#39;</span><span class="p">];</span>
 <span class="nv">$sperreart</span> <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">&#39;sperreart&#39;</span><span class="p">];</span>
 <span class="nv">$begruendung</span> <span class="o">=</span> <span class="nv">$_POST</span><span class="p">[</span><span class="s1">&#39;begruendung&#39;</span><span class="p">];</span>
 <span class="nv">$datum</span> <span class="o">=</span> <span class="nb">date</span><span class="p">(</span><span class="s2">&quot;Y-m-d H:i:s&quot;</span><span class="p">);</span>

 <span class="nv">$sql</span> <span class="o">=</span> <span class="s2">&quot;INSERT INTO tbl_formulardaten(Anrede, Vorname, Nachname, Strasse, PLZ, Stadt, EMail, SperreArt, Begruendung, Uhrzeit) VALUES (:anrede, :vorname, :nachname, :strasse, :plz, :stadt, :email, :sperre, :begruendung, :uhrzeit)&quot;</span><span class="p">;</span>
 <span class="nv">$stmt</span> <span class="o">=</span> <span class="nv">$dbh</span><span class="o">-&gt;</span><span class="na">prepare</span><span class="p">(</span><span class="nv">$sql</span><span class="p">);</span>
 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">bindValue</span><span class="p">(</span><span class="s1">&#39;:anrede&#39;</span><span class="p">,</span> <span class="nv">$anrede</span><span class="p">);</span>
 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">bindValue</span><span class="p">(</span><span class="s1">&#39;:vorname&#39;</span><span class="p">,</span> <span class="nv">$vorname</span><span class="p">);</span>
 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">bindValue</span><span class="p">(</span><span class="s1">&#39;:nachname&#39;</span><span class="p">,</span> <span class="nv">$nachname</span><span class="p">);</span>
 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">bindValue</span><span class="p">(</span><span class="s1">&#39;:strasse&#39;</span><span class="p">,</span> <span class="nv">$straßeundhausnr</span><span class="p">);</span>
 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">bindValue</span><span class="p">(</span><span class="s1">&#39;:plz&#39;</span><span class="p">,</span> <span class="nv">$plz</span><span class="p">);</span>
 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">bindValue</span><span class="p">(</span><span class="s1">&#39;:stadt&#39;</span><span class="p">,</span> <span class="nv">$stadt</span><span class="p">);</span>
 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">bindValue</span><span class="p">(</span><span class="s1">&#39;:email&#39;</span><span class="p">,</span> <span class="nv">$email</span><span class="p">);</span>
 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">bindValue</span><span class="p">(</span><span class="s1">&#39;:sperre&#39;</span><span class="p">,</span> <span class="nv">$sperreart</span><span class="p">);</span>
 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">bindValue</span><span class="p">(</span><span class="s1">&#39;:begruendung&#39;</span><span class="p">,</span> <span class="nv">$begruendung</span><span class="p">);</span>
 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">bindValue</span><span class="p">(</span><span class="s1">&#39;:uhrzeit&#39;</span><span class="p">,</span> <span class="nv">$datum</span><span class="p">);</span>

 <span class="nv">$stmt</span><span class="o">-&gt;</span><span class="na">execute</span><span class="p">();</span>
<span class="p">}</span>
<span class="cp">?&gt;</span><span class="x"></span>
</pre></div>
</div>
<p>Dieses PHP-Skript überprüft im ersten Schritt, ob der Button im Formular mit dem Namen „abschicken“ betätigt wurde. Ist dies der Fall, startet die Schleife.
Um weitere Schritte auszuführen, ist eine Datei unter „inc/db_connect.php“ required. Diese Datei wird im Folgenden erklärt. Sie stellt die Verbindung zur Datenbank her, in welche die Eingaben aus dem Formular eingetragen werden sollen.
Anschließend bekommen alle Formularfelder, welche über die _POST-Methode Ihre Daten übermittelt, eine eigene Variable z.B. $anrede. Dies wird für alle Formularfelder im oberen Block des PHP-Skripts getan.
Im Zweiten Block wird nach der $sql-Variable der SQL-Befehl für die Datenbank vermerkt und an die db_connect.php-Datei weitergeben, welche dies Vorbereitet.
Nachfolgend werden die Inhalte der Variablen z.B. $anrede an die Datenbankspalte :anrede gebunden. Dies geschieht im Rahmen einer neuen Variable.</p>
<p>Anschließend werden in der letzten Zeile, alle Befehle ausgeführt.</p>
<p><strong>/inc/db_connect.php</strong></p>
<p>Diese Datei hat folgenden Inhalt:</p>
<div class="highlight-php notranslate"><div class="highlight"><pre><span></span><span class="cp">&lt;?PHP</span>

<span class="nv">$user</span> <span class="o">=</span> <span class="s2">&quot;NUTZERNAME&quot;</span><span class="p">;</span>
<span class="nv">$pass</span> <span class="o">=</span> <span class="s2">&quot;PASSWORT&quot;</span><span class="p">;</span>

<span class="k">try</span>
<span class="p">{</span>
<span class="nv">$dbh</span> <span class="o">=</span> <span class="k">new</span> <span class="nx">PDO</span><span class="p">(</span><span class="s1">&#39;mysql:host=localhost;dbname=melderegistersperre;charset=utf8&#39;</span><span class="p">,</span> <span class="nv">$user</span><span class="p">,</span> <span class="nv">$pass</span><span class="p">);</span>
<span class="nv">$dbh</span><span class="o">-&gt;</span><span class="na">setAttribute</span><span class="p">(</span><span class="nx">PDO</span><span class="o">::</span><span class="na">ATTR_ERRMODE</span><span class="p">,</span> <span class="nx">PDO</span><span class="o">::</span><span class="na">ERRMODE_EXCEPTION</span><span class="p">);</span>
<span class="p">}</span>
<span class="k">catch</span> <span class="p">(</span><span class="nx">PDOException</span> <span class="nv">$e</span><span class="p">)</span>
<span class="p">{</span>
 <span class="k">print</span> <span class="s2">&quot;Error!: &quot;</span> <span class="o">.</span> <span class="nv">$e</span><span class="o">-&gt;</span><span class="na">getMessage</span><span class="p">()</span> <span class="o">.</span> <span class="s2">&quot;&lt;br/&gt;&quot;</span><span class="p">;</span>
 <span class="k">die</span><span class="p">();</span>
<span class="p">}</span>
<span class="cp">?&gt;</span><span class="x"></span>
</pre></div>
</div>
<p>In dieser werden im ersten Schritt Variablen für den Login eines Benutzers auf der Datenbank festgelegt. Diese db_connect-Datei ist über den Server als einzelne nicht aufrufbar und auch nicht auslesbar. Somit bleiben deren Inhalte geheim.
Im try-Block versucht das Skript eine Verbindung zu angebeben Datenbank herzustellen. Sollte dies nicht gelingen, wird der Fehler über catch-Anweisungen abgefangen.
Im unteren Abschnitt dieser Dokumentation wird ein SQL-Skript eingefügt, welches Sie in Ihre Datenbank importieren bzw. eingeben können, damit die korrekten Datenbanktabellen etc. automatisch erstellt werden. Die Benutzer, Datenbanken und Tabellen müssen natürlich mit dieser db_connect-Datei korrelieren.</p>
<p><strong>login.php</strong></p>
<p>Diese Seite/Datei ist für Sachbearbeiter vorgesehen. Diese können sich nur mit Einlogg-Daten anmelden. Daten liegen hierfür in einer seperaten Datenbank bereit. Diese Datenbank enthält eine Tabelle mit Informationen über Benutzername, Passwort und Admin-Status. Wie diese Datenbank erstellt wird, erfahren Sie im Folgenden.
Das Konto für die Erstverwendung lautet:</p>
<blockquote>
<div><p>Name: admin
Passwort: phpadmin</p>
<p>Diese Daten müssen in der Login-Datenbank umgehend geändert werden.</p>
</div></blockquote>
<p>Anschließend lassen sich im Dashboard, welches sich nach erfolgreichem Login öffnet, neue Benutzer im Reiter links anlegen. Das Passwort wird im Eingabefeld im Klartext eingegeben und der erzeugte Hashwert an den Datenbank-Adminsitrator übermittelt.
Dieser legt einen neuen Benutzer an, in dem einen gewählten Nutzernamen und den übermittelten Hash-Wert in die Datenbank-Login-Tabelle einfügt.</p>
<p><strong>/login/dashboard.php</strong>
Das Dashboard ist nur nach erfolgreichem Login über die login.php aufrufbar. Hier werden Eingaben aus dem Melderegistersperre-Formular angezeigt und können vom Sachbearbeiter bearbeitet werden.
Wie die dashboard.php-Datei mit Ihrer Datenbank auf Ihrem Server verknüpft wird, wird weiter unten beschrieben.
Nach Logout aus dem Dashboard werden sie zur index.php der Melderegistersperre-Website weitergeleitet und haben keinen Zugriff mehr auf das Dashboard.</p>
<div class="section" id="die-melderegistersperre-formular-datenbank-verknupfen">
<h3><a class="toc-backref" href="#id11">6.1 Die Melderegistersperre-Formular-Datenbank verknüpfen</a><a class="headerlink" href="#die-melderegistersperre-formular-datenbank-verknupfen" title="Link zu dieser Überschrift">¶</a></h3>
<p>Die Daten welche über das Formular auf der Website eingegeben werden, werden an eine Datenbank weitergeleitet. Diese Datenbank muss erstellt werden, mit folgendem Befehl, welcher in Ihre Datenbank-Admin-Oberfläche eingebeben werden muss oder:
Eine formulareingabe_datenbank_SQL.sql-Datei ist im Repository vorhanden und muss nur in die PhpMyAdmin-Oberfläche über den Reiter „Importieren“ importiert werden. Die Datenbank, sämtliche Tabellen usw. werden somit automatisch erstellt.</p>
<div class="highlight-sql notranslate"><div class="highlight"><pre><span></span><span class="k">SET</span> <span class="n">SQL_MODE</span> <span class="o">=</span> <span class="ss">&quot;NO_AUTO_VALUE_ON_ZERO&quot;</span><span class="p">;</span>
<span class="k">SET</span> <span class="n">AUTOCOMMIT</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span>
<span class="k">START</span> <span class="n">TRANSACTION</span><span class="p">;</span>
<span class="k">SET</span> <span class="n">time_zone</span> <span class="o">=</span> <span class="ss">&quot;+00:00&quot;</span><span class="p">;</span>

<span class="k">CREATE</span> <span class="k">DATABASE</span> <span class="k">IF</span> <span class="k">NOT</span> <span class="k">EXISTS</span> <span class="o">`</span><span class="n">melderegistersperre</span><span class="o">`</span> <span class="k">DEFAULT</span> <span class="nb">CHARACTER</span> <span class="k">SET</span> <span class="n">latin1</span> <span class="k">COLLATE</span> <span class="n">latin1_german2_ci</span><span class="p">;</span>
<span class="n">USE</span> <span class="o">`</span><span class="n">melderegistersperre</span><span class="o">`</span><span class="p">;</span>

<span class="k">CREATE</span> <span class="k">TABLE</span> <span class="o">`</span><span class="n">tbl_formulardaten</span><span class="o">`</span> <span class="p">(</span>
<span class="o">`</span><span class="n">id</span><span class="o">`</span> <span class="n">mediumint</span><span class="p">(</span><span class="mi">9</span><span class="p">)</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">Anrede</span><span class="o">`</span> <span class="nb">varchar</span><span class="p">(</span><span class="mi">20</span><span class="p">)</span> <span class="k">COLLATE</span> <span class="n">utf8_german2_ci</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">Vorname</span><span class="o">`</span> <span class="nb">varchar</span><span class="p">(</span><span class="mi">100</span><span class="p">)</span> <span class="k">COLLATE</span> <span class="n">utf8_german2_ci</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">Nachname</span><span class="o">`</span> <span class="nb">varchar</span><span class="p">(</span><span class="mi">120</span><span class="p">)</span> <span class="k">COLLATE</span> <span class="n">utf8_german2_ci</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">Strasse</span><span class="o">`</span> <span class="nb">varchar</span><span class="p">(</span><span class="mi">150</span><span class="p">)</span> <span class="k">COLLATE</span> <span class="n">utf8_german2_ci</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">PLZ</span><span class="o">`</span> <span class="nb">int</span><span class="p">(</span><span class="mi">11</span><span class="p">)</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">Stadt</span><span class="o">`</span> <span class="nb">varchar</span><span class="p">(</span><span class="mi">100</span><span class="p">)</span> <span class="k">COLLATE</span> <span class="n">utf8_german2_ci</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">SperreArt</span><span class="o">`</span> <span class="nb">varchar</span><span class="p">(</span><span class="mi">50</span><span class="p">)</span> <span class="k">COLLATE</span> <span class="n">utf8_german2_ci</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">EMail</span><span class="o">`</span> <span class="nb">varchar</span><span class="p">(</span><span class="mi">120</span><span class="p">)</span> <span class="k">COLLATE</span> <span class="n">utf8_german2_ci</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">Begruendung</span><span class="o">`</span> <span class="nb">varchar</span><span class="p">(</span><span class="mi">5000</span><span class="p">)</span> <span class="k">COLLATE</span> <span class="n">utf8_german2_ci</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">Uhrzeit</span><span class="o">`</span> <span class="n">datetime</span> <span class="k">NOT</span> <span class="k">NULL</span>
<span class="p">)</span> <span class="n">ENGINE</span><span class="o">=</span><span class="n">InnoDB</span> <span class="k">DEFAULT</span> <span class="n">CHARSET</span><span class="o">=</span><span class="n">utf8</span> <span class="k">COLLATE</span><span class="o">=</span><span class="n">utf8_german2_ci</span><span class="p">;</span>

<span class="k">ALTER</span> <span class="k">TABLE</span> <span class="o">`</span><span class="n">tbl_formulardaten</span><span class="o">`</span>
<span class="k">ADD</span> <span class="k">PRIMARY</span> <span class="k">KEY</span> <span class="p">(</span><span class="o">`</span><span class="n">id</span><span class="o">`</span><span class="p">);</span>

<span class="k">ALTER</span> <span class="k">TABLE</span> <span class="o">`</span><span class="n">tbl_formulardaten</span><span class="o">`</span>
<span class="k">MODIFY</span> <span class="o">`</span><span class="n">id</span><span class="o">`</span> <span class="n">mediumint</span><span class="p">(</span><span class="mi">9</span><span class="p">)</span> <span class="k">NOT</span> <span class="k">NULL</span> <span class="n">AUTO_INCREMENT</span><span class="p">,</span> <span class="n">AUTO_INCREMENT</span><span class="o">=</span><span class="mi">16</span><span class="p">;</span>
<span class="k">COMMIT</span><span class="p">;</span>
</pre></div>
</div>
<p>Die Datenbank ist nun erstellt. Sie müssen nun noch einen Benutzer auf dieser Datenbank anlegen. Dessen Nutzername und Passwort muss in die db_connect.php-Datei in den ersten beiden Variablen $user und $pass jeweils eingetragen werden.
Die Datenbank ist nun verknüpft.</p>
</div>
<div class="section" id="die-login-datenbank-erstellen">
<h3><a class="toc-backref" href="#id12">6.2 Die Login-Datenbank erstellen</a><a class="headerlink" href="#die-login-datenbank-erstellen" title="Link zu dieser Überschrift">¶</a></h3>
<p>Um einen Login in das Dashboard zu ermöglichen, muss Folgende Datenbank wie oben genannt erstellt werden:
Eine login_datenbank_SQL.sql-Datei ist im Repository vorhanden und muss nur in die PhpMyAdmin-Oberfläche über den Reiter „Importieren“ importiert werden. Die Datenbank, sämtliche Tabellen usw. werden somit automatisch erstellt.</p>
<div class="highlight-sql notranslate"><div class="highlight"><pre><span></span><span class="k">SET</span> <span class="n">SQL_MODE</span> <span class="o">=</span> <span class="ss">&quot;NO_AUTO_VALUE_ON_ZERO&quot;</span><span class="p">;</span>
<span class="k">SET</span> <span class="n">AUTOCOMMIT</span> <span class="o">=</span> <span class="mi">0</span><span class="p">;</span>
<span class="k">START</span> <span class="n">TRANSACTION</span><span class="p">;</span>
<span class="k">SET</span> <span class="n">time_zone</span> <span class="o">=</span> <span class="ss">&quot;+00:00&quot;</span><span class="p">;</span>

<span class="k">CREATE</span> <span class="k">DATABASE</span> <span class="k">IF</span> <span class="k">NOT</span> <span class="k">EXISTS</span> <span class="o">`</span><span class="n">secure_login</span><span class="o">`</span> <span class="k">DEFAULT</span> <span class="nb">CHARACTER</span> <span class="k">SET</span> <span class="n">latin1</span> <span class="k">COLLATE</span> <span class="n">latin1_german2_ci</span><span class="p">;</span>
<span class="n">USE</span> <span class="o">`</span><span class="n">secure_login</span><span class="o">`</span><span class="p">;</span>

<span class="k">CREATE</span> <span class="k">TABLE</span> <span class="o">`</span><span class="n">tbl_users</span><span class="o">`</span> <span class="p">(</span>
<span class="o">`</span><span class="n">username</span><span class="o">`</span> <span class="nb">varchar</span><span class="p">(</span><span class="mi">50</span><span class="p">)</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="n">password</span><span class="o">`</span> <span class="nb">varchar</span><span class="p">(</span><span class="mi">255</span><span class="p">)</span> <span class="k">NOT</span> <span class="k">NULL</span><span class="p">,</span>
<span class="o">`</span><span class="k">admin</span><span class="o">`</span> <span class="n">tinyint</span><span class="p">(</span><span class="mi">1</span><span class="p">)</span> <span class="k">NOT</span> <span class="k">NULL</span> <span class="k">DEFAULT</span> <span class="mi">0</span>
<span class="p">)</span> <span class="n">ENGINE</span><span class="o">=</span><span class="n">InnoDB</span> <span class="k">DEFAULT</span> <span class="n">CHARSET</span><span class="o">=</span><span class="n">utf8</span><span class="p">;</span>


<span class="k">INSERT</span> <span class="k">INTO</span> <span class="o">`</span><span class="n">tbl_users</span><span class="o">`</span> <span class="p">(</span><span class="o">`</span><span class="n">username</span><span class="o">`</span><span class="p">,</span> <span class="o">`</span><span class="n">password</span><span class="o">`</span><span class="p">,</span> <span class="o">`</span><span class="k">admin</span><span class="o">`</span><span class="p">)</span> <span class="k">VALUES</span>
<span class="p">(</span><span class="s1">&#39;admin&#39;</span><span class="p">,</span> <span class="s1">&#39;$2y$10$A/uCY4Q.B/zEB.Za68hw6Ol9E86wLjP1b2WqOJR6.hRh3h2HCaeB2&#39;</span><span class="p">,</span> <span class="mi">1</span><span class="p">),</span>

<span class="k">ALTER</span> <span class="k">TABLE</span> <span class="o">`</span><span class="n">tbl_users</span><span class="o">`</span>
<span class="k">ADD</span> <span class="k">PRIMARY</span> <span class="k">KEY</span> <span class="p">(</span><span class="o">`</span><span class="n">username</span><span class="o">`</span><span class="p">);</span>
<span class="k">COMMIT</span><span class="p">;</span>
</pre></div>
</div>
<p>Die Datenbank wurde nun erstellt und ein Login durch den Erstbenutzer ist möglich.
Name: admin
Passwort: phpadmin
Der Account sollte umgehend aus der Datenbank entfernt werden,  nachdem ein neuer Benutzer mit anderem Passwort (Hashwert) angelegt wurde.</p>
</div>
<div class="section" id="das-dashboard-mit-der-melderegister-formular-datenbank-verbinden">
<h3><a class="toc-backref" href="#id13">6.3 Das Dashboard mit der Melderegister-Formular-Datenbank verbinden</a><a class="headerlink" href="#das-dashboard-mit-der-melderegister-formular-datenbank-verbinden" title="Link zu dieser Überschrift">¶</a></h3>
<p>Auf der Login-Datenbank muss ein Nutzer erstellt sein, welcher den Namen „root“ und das Passwort „phpadmin“ trägt. Ist dies gegeben, ist die Datenbank nun verknüpft.
Soll Benutzername und Passwort geändert werden, geschieht dies in der Datei unter /login-system/inc/db.php
Die zuändernden Stellen sind eindeutig zu erkennen.</p>
</div>
</div>
<div class="section" id="akteure">
<h2><a class="toc-backref" href="#id14">7. Akteure</a><a class="headerlink" href="#akteure" title="Link zu dieser Überschrift">¶</a></h2>
<p>Beteiligte Akteure und Ihre Use Cases sind im folgenden UML-Diagramm dargestellt.</p>
<div class="figure align-center" id="uml-diagramm">
<a class="reference internal image-reference" href="../_images/umldiagramm.png"><img alt="UML-Diagramm" src="../_images/umldiagramm.png" style="width: 100%;" /></a>
<p class="caption"><span class="caption-text">UML-Diagramm der Akteure und Use-Cases</span><a class="headerlink" href="#uml-diagramm" title="Link zu diesem Bild">¶</a></p>
</div>
</div>
<div class="section" id="daten-als-xml-exportieren">
<h2><a class="toc-backref" href="#id15">8. Daten als XML exportieren</a><a class="headerlink" href="#daten-als-xml-exportieren" title="Link zu dieser Überschrift">¶</a></h2>
<p>Der Export der Eingebenen Formulardaten erfolgt in unserem Service lediglich über die Datenbankverwaltung z.B. phpMyAdmin. Hierfür wird die zu exportierende Datenbank ausgewählt und in der oberen Leiste von phpMyAdmin auf „Exportieren“ geklickt.
Im Unteren Bereich der Export-Seite wird Format „XML“ ausgewählt und mit „Ok“ bestätigt.
Ein Export der Daten als XML steht nun zum Download bereit, welcher automatisch starten sollte.
Eine weitere Exportmöglichkeit ist derzeit nicht vorhanden, wurde in der Aufgabenstellung auch nicht näher definiert.</p>
</div>
<div class="section" id="anleitung-fur-burger">
<h2><a class="toc-backref" href="#id16">9. Anleitung für Bürger</a><a class="headerlink" href="#anleitung-fur-burger" title="Link zu dieser Überschrift">¶</a></h2>
<p>Die Anleitung für Bürger hält sich aufgrund der Einfachheit des Services relativ überschaubar.
Über den Button „Antrag stellen“ oben rechts, rot hervorgehoben, kann der Bürger seinen Antrag stellen.
Auf der Antragseite findet er ein Formular vor, in welches er seine Daten einträgt. Die Daten werden validiert und auf Sinnhaftigkeit überprüft.
Über den Button „Absenden“ wird der Antrag an die Behörde weitergeleitet.
Der Vorgang ist an dieser Stelle für den Bürger beendet.</p>
</div>
<div class="section" id="pyhton-installieren">
<h2><a class="toc-backref" href="#id17">10. Pyhton installieren</a><a class="headerlink" href="#pyhton-installieren" title="Link zu dieser Überschrift">¶</a></h2>
<p>Python installieren:
.. <a class="reference external" href="https://www.python.org/downloads/">https://www.python.org/downloads/</a></p>
<p>Ein für die Installation von Sphinx notwendiger Zustand ist die Erstellung und Aktivierung eines Environments:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>py -3 -m venv env
</pre></div>
</div>
<p>Gleiches wird über</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>.<span class="se">\e</span>nv<span class="se">\S</span>cripts<span class="se">\a</span>ctivate.bat
</pre></div>
</div>
<p>aktviert.</p>
</div>
<div class="section" id="sphinx-installieren">
<h2><a class="toc-backref" href="#id18">11. Sphinx installieren</a><a class="headerlink" href="#sphinx-installieren" title="Link zu dieser Überschrift">¶</a></h2>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>$ pip install sphinx
</pre></div>
</div>
<p>Dokumentation erstellen:</p>
<div class="highlight-bash notranslate"><table class="highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre>1
2
3</pre></div></td><td class="code"><div class="highlight"><pre><span></span>$ mkdir dokumentation
$ <span class="nb">cd</span> dokumentation
$ sphinx-quickstart
</pre></div>
</td></tr></table></div>
<p>HTML erzeugen lassen:</p>
<div class="highlight-bash notranslate"><div class="highlight"><pre><span></span>$ make html
</pre></div>
</div>
</div>
</div>


          </div>
          
        </div>
      </div>
    


</ul>
</li>
</ul>
</div>
<div class="section" id="git-verwenden-upload-und-download">
<h2><a class="toc-backref" href="#id2">Git verwenden (Upload und Download)</a><a class="headerlink" href="#git-verwenden-upload-und-download" title="Link zu dieser Überschrift">¶</a></h2>
<p>Ein vorhandener Git-Account wird hierfür vorausgesetzt.</p>
<p>Folgende Schritte müssen nun ausgeführt werden:</p>
<div class="highlight-bash notranslate"><table class="highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre>1
2
3</pre></div></td><td class="code"><div class="highlight"><pre><span></span>$ Git global setup
$ git config --global user.name <span class="s2">&quot;Vorname Nachname&quot;</span>
$ git config --global user.email <span class="s2">&quot;mail@adresse.de&quot;</span>
</pre></div>
</td></tr></table></div>
<p>Create a new repository</p>
<div class="highlight-bash notranslate"><table class="highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre>1
2
3
4
5
6</pre></div></td><td class="code"><div class="highlight"><pre><span></span>$ git clone git@gitlab.com:NUTZERNAME/PROJEKTNAME.git
$ <span class="nb">cd</span> PROJEKTNAME
$ touch README.md
$ git add README.md
$ git commit -m <span class="s2">&quot;add README&quot;</span>
$ git push -u origin master
</pre></div>
</td></tr></table></div>
<p>Push an existing folder</p>
<div class="highlight-bash notranslate"><table class="highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre>1
2
3
4
5
6</pre></div></td><td class="code"><div class="highlight"><pre><span></span>$ <span class="nb">cd</span> existing_folder
$ git init
$ git remote add origin git@gitlab.com:pittiplatsch1/ozg_sperre.git
$ git add .
$ git commit -m <span class="s2">&quot;Initial commit&quot;</span>
$ git push -u origin master
</pre></div>
</td></tr></table></div>
<p>Push an existing Git repository</p>
<div class="highlight-bash notranslate"><table class="highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre>1
2
3
4
5</pre></div></td><td class="code"><div class="highlight"><pre><span></span>$ <span class="nb">cd</span> existing_repo
$ git remote rename origin old-origin
$ git remote add origin git@gitlab.com:pittiplatsch1/ozg_sperre.git
$ git push -u origin --all
$ git push -u origin --tags
</pre></div>
</td></tr></table></div>
</div>
</div>


          </div>
          
        </div>
      </div>





        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2019, Christoph Wolf.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 2.2.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.12</a>
      
      |
      <a href="_sources/master.rst.txt"
          rel="nofollow">Page source</a>
    </div>


                </div>
            </div>


        </div>
    </section>

    <!-- /.container -->


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


    <footer class="pb-4 bg-primary-3 text-light" id="footer">
        <div class="container">
            <div class="row mb-5">
                <div class="col">

                </div>
            </div>
            <div class="row mb-5">
                <div class="col-6 col-lg-3 col-xl-2">
                    <h5>Navigation</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a href="hilfe.php" class="nav-link">Hilfe</a>
                        </li>
                        <li class="nav-item">
                            <a href="impressum.php" class="nav-link">Impressum</a>
                        </li>
                        <li class="nav-item">
                            <a href="login-system/login.php" class="nav-link">Login</a>
                        </li>
                    </ul>
                </div>
                <div class="col-6 col-lg">
                    <h5>Kontakt</h5>
                    <ul class="list-unstyled">
                        <li class="mb-3 d-flex">
                            <img class="icon" src="assets/img/icons/theme/map/marker-1.svg" alt="marker-1 icon"
                                data-inject-svg />
                            <div class="ml-3">
                                <span>Musterstraße 1
                                    <br />Wildau, Brandenburg</span>
                            </div>
                        </li>
                        <li class="mb-3 d-flex">
                            <img class="icon" src="assets/img/icons/theme/communication/call-1.svg" alt="call-1 icon"
                                data-inject-svg />
                            <div class="ml-3">
                                <span>+49 1234 56789</span>
                                <span class="d-block text-muted text-small">Mo - Fr 9:00 - 17:00</span>
                            </div>
                        </li>
                        <li class="mb-3 d-flex">
                            <img class="icon" src="assets/img/icons/theme/communication/mail.svg" alt="mail icon"
                                data-inject-svg />
                            <div class="ml-3">
                                <a href="#">hello@verwaltung.de</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-5 col-xl-4 mt-3 mt-lg-0">

                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col col-md-auto text-center">
                    <small class="text-muted">&copy;2019 - Christoph Wolf, Jannik Walter, Ole Schwarz, Alexander
                        Schröder<a href="https://www.google.com/policies/privacy/"> Privacy Policy</a> und <a
                            href="https://policies.google.com/terms">Terms of Service.</a>
                    </small>
                </div>
            </div>
        </div>
    </footer>
    <a href="#" class="btn back-to-top btn-primary btn-round" data-smooth-scroll data-aos="fade-up"
        data-aos-offset="2000" data-aos-mirror="true" data-aos-once="false">
        <img class="icon" src="assets/img/icons/theme/navigation/arrow-up.svg" alt="arrow-up icon" data-inject-svg />
    </a>
    <!-- Required vendor scripts (Do not remove) -->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>

    <!-- Optional Vendor Scripts (Remove the plugin script here and comment initializer script out of index.js if site does not use that feature) -->

    <!-- AOS (Animate On Scroll - animates elements into view while scrolling down) -->
    <script type="text/javascript" src="assets/js/aos.js"></script>
    <!-- Clipboard (copies content from browser into OS clipboard) -->
    <script type="text/javascript" src="assets/js/clipboard.js"></script>
    <!-- Fancybox (handles image and video lightbox and galleries) -->
    <script type="text/javascript" src="assets/js/jquery.fancybox.min.js"></script>
    <!-- Flatpickr (calendar/date/time picker UI) -->
    <script type="text/javascript" src="assets/js/flatpickr.min.js"></script>
    <!-- Flickity (handles touch enabled carousels and sliders) -->
    <script type="text/javascript" src="assets/js/flickity.pkgd.min.js"></script>
    <!-- Ion rangeSlider (flexible and pretty range slider elements) -->
    <script type="text/javascript" src="assets/js/ion.rangeSlider.min.js"></script>
    <!-- Isotope (masonry layouts and filtering) -->
    <script type="text/javascript" src="assets/js/isotope.pkgd.min.js"></script>
    <!-- jarallax (parallax effect and video backgrounds) -->
    <script type="text/javascript" src="assets/js/jarallax.min.js"></script>
    <script type="text/javascript" src="assets/js/jarallax-video.min.js"></script>
    <script type="text/javascript" src="assets/js/jarallax-element.min.js"></script>
    <!-- jQuery Countdown (displays countdown text to a specified date) -->
    <script type="text/javascript" src="assets/js/jquery.countdown.min.js"></script>
    <!-- jQuery smartWizard facilitates steppable wizard content -->
    <script type="text/javascript" src="assets/js/jquery.smartWizard.min.js"></script>
    <!-- Plyr (unified player for Video, Audio, Vimeo and Youtube) -->
    <script type="text/javascript" src="assets/js/plyr.polyfilled.min.js"></script>
    <!-- Prism (displays formatted code boxes) -->
    <script type="text/javascript" src="assets/js/prism.js"></script>
    <!-- ScrollMonitor (manages events for elements scrolling in and out of view) -->
    <script type="text/javascript" src="assets/js/scrollMonitor.js"></script>
    <!-- Smooth scroll (animation to links in-page)-->
    <script type="text/javascript" src="assets/js/smooth-scroll.polyfills.min.js"></script>
    <!-- SVGInjector (replaces img tags with SVG code to allow easy inclusion of SVGs with the benefit of inheriting colors and styles)-->
    <script type="text/javascript" src="assets/js/svg-injector.umd.production.js"></script>
    <!-- TwitterFetcher (displays a feed of tweets from a specified account)-->
    <script type="text/javascript" src="assets/js/twitterFetcher_min.js"></script>
    <!-- Typed text (animated typing effect)-->
    <script type="text/javascript" src="assets/js/typed.min.js"></script>
    <!-- Required theme scripts (Do not remove) -->
    <script type="text/javascript" src="assets/js/theme.js"></script>
    <!-- Removes page load animation when window is finished loading -->
    <script type="text/javascript">
        window.addEventListener("load", function () { document.querySelector('body').classList.add('loaded'); });
    </script>

</body>

</html>