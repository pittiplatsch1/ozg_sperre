<?php
include("../inc/config.php");

try 
{
   $dbh = new PDO('mysql:host='.HOST.';dbname='.DATABASE.'', USER);
} 
catch (PDOException $e)
{
   print "Error!: " . $e->getMessage() . "<br/>";
   die();
}
?>