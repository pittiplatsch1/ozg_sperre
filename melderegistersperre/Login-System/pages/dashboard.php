<?php
session_start();
if(!isset($_SESSION['user'], $_SESSION['logged_in']))
{
	header('Location: ../index.php');
	exit;
}
?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Die 3 Meta-Tags oben *müssen* zuerst im head stehen; jeglicher sonstiger head-Inhalt muss *nach* diesen Tags kommen -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Dashboard</title>

    <!-- Bootstrap-CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Besondere Stile für diese Vorlage -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Nur für Testzwecke. Kopiere diese Zeilen nicht in echte Projekte! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- Unterstützung für Media Queries und HTML5-Elemente in IE8 über HTML5 shim und Respond.js -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
            <span class="sr-only">Navigation ein-/ausblenden</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Bearbeitungssystem der Melderegistersperre des Landes Schleswig-Holstein</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="#">Hilfe</a></li>
			<li><a href="../func/logout.php">Logout</a></li>
          </ul>
          <form class="navbar-form navbar-right">
          
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="dashboard.php">Überblick <span class="sr-only">(aktuell)</span></a></li>
            <li class="active"><a href="newuser.php">Neuer Bearbeiter <span class="sr-only">(aktuell)</span></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Dashboard</h1>
		
          <table class="table table-sm">
<tr>
<th>ID</th>
<th>Anrede</th>
<th>Vorname</th>
<th>Nachname</th>
<th>Strasse</th>
<th>PLZ</th>
<th>Stadt</th>
<th>Sperre Art</th>
<th>E-Mail</th>
<th>Begründung</th>
<th>Uhrzeit</th>
</tr>
<?php
$conn = mysqli_connect("localhost", "root", "phpadmin", "melderegistersperre");
// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT id, anrede, vorname, nachname, strasse, plz, stadt, sperreart, email, begruendung, uhrzeit FROM tbl_formulardaten";
$result = $conn->query($sql);

// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["id"] . "</td><td>" . $row["anrede"] . "</td><td>"
. $row["vorname"] . "</td><td>" . $row["nachname"] . "</td><td>" . $row["strasse"] . "</td><td>"
. $row["plz"] . "</td><td>" . $row["stadt"]. "</td><td>" . $row["sperreart"] . "</td><td>"
. $row["email"] . "</td><td>" . $row["begruendung"] . "</td><td>". $row["uhrzeit"] . "</td><tr>";

}
echo "</table>";

$conn->close();
?>
</table>
        </div>
      </div>
    </div>

    <!-- Bootstrap-JavaScript
    ================================================== -->
    <!-- Am Ende des Dokuments platziert, damit Seiten schneller laden -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <!-- Nur, um unsere Platzhalter-Bilder zum Laufen zu bringen. Die nächste Zeile nicht wirklich kopieren! -->
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <!-- IE10-Anzeigefenster-Hack für Fehler auf Surface und Desktop-Windows-8 -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
