<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/favicon.ico">

    <title>Login für Sachbearbeiter</title>

	
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/signin.css" rel="stylesheet">
	
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <form class="form-signin" action="func/login.php" method="POST">
        <h2 class="form-signin-heading">Login für Sachbearbeiter</h2>
        
        <label for="frm_user" class="sr-only">Benutzername</label>
        <input type="text" name="frm_user" id="frm_user" class="form-control" placeholder="Benutzer" required autofocus>
        <label for="frm_psw" class="sr-only">Passwort</label>
        <input type="password" name="frm_psw" id="frm_psw" class="form-control" placeholder="Passwort" required>
        <button class="btn btn-secondary btn-lg" name="btn_login" type="submit">Login</button>
      </form>
    </div>
  </body>
</html>
