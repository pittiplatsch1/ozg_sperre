<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8"/>
<title>Table with database</title>
<style>

</style>
</head>
<body>
<h1>Antragsteller</h1>
<table>
<tr>
<th>ID</th>
<th>Anrede</th>
<th>Vorname</th>
<th>Nachname</th>
<th>Strasse</th>
<th>PLZ</th>
<th>Stadt</th>
<th>Sperre Art</th>
<th>E-Mail</th>
<th>Begründung</th>
<th>Uhrzeit</th>
</tr>
<?php
$conn = mysqli_connect("localhost", "root", "phpadmin", "melderegistersperre");
// Check connection
if ($conn->connect_error) {
die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT id, anrede, vorname, nachname, strasse, plz, stadt, sperreart, email, begruendung, uhrzeit FROM tbl_formulardaten";
$result = $conn->query($sql);

// output data of each row
while($row = $result->fetch_assoc()) {
echo "<tr><td>" . $row["id"] . "</td><td>" . $row["anrede"] . "</td><td>"
. $row["vorname"] . "</td><td>" . $row["nachname"] . "</td><td>" . $row["strasse"] . "</td><td>"
. $row["plz"] . "</td><td>" . $row["stadt"]. "</td><td>" . $row["sperreart"] . "</td><td>"
. $row["email"] . "</td><td>" . $row["begruendung"] . "</td><td>". $row["uhrzeit"] . "</td><tr>";

}
echo "</table>";

$conn->close();
?>
</table>
</body>
</html>