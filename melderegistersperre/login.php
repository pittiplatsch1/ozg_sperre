<?php
if(isset($_POST['btn_login'], $_POST['frm_user'], $_POST['frm_psw']))
{
	include("../inc/db_connect.php");
	
	$frm_user = trim($_POST['frm_user']);
	$frm_psw = trim($_POST['frm_psw']);
	
	$stmt = $dbh->prepare("SELECT password, admin FROM tbl_users u WHERE u.username = :frm_user LIMIT 1");
	$stmt->bindParam(':frm_user', $frm_user);
	$stmt->execute();
	
	while ($row = $stmt->fetch()) 
	{
		$db_psw = $row['password'];

		//$passwort_für_datenbank = password_hash("gazica", PASSWORD_DEFAULT);
		//$2y$10$QOgWCGBL8E/tQwFNx1dIseINBQVEhPZypJajTRPgVisugmsKEADky
		
    	if (password_verify($frm_psw, $db_psw)) 
		{
			session_start();
			
			$_SESSION['user'] = $frm_user;
			$_SESSION['logged_in'] = TRUE;
			$_SESSION['admin'] = $row['admin'];
			
			header('Location: ../pages/dashboard.php');
			exit;
		}
		else
		{
			echo "Das Kennwort ist nicht korrekt";
		}
  	}
}
?>