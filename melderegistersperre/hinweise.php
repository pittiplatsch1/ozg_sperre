<!doctype html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <title>Melderegistersperre</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A corporate Bootstrap theme by Medium Rare">
    <link href="assets/css/loaders/loader-typing.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/css/theme-desktop-app.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="preload" as="font" href="assets/fonts/Inter-UI-upright.var.woff2" type="font/woff2"
        crossorigin="anonymous">
    <link rel="preload" as="font" href="assets/fonts/Inter-UI.var.woff2" type="font/woff2" crossorigin="anonymous">
</head>

<body>
    <div class="loader">
        <div class="loading-animation"></div>
    </div>

    <div class="navbar-container bg-primary-3">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary-3" data-sticky="top">
            <div class="container">
                <a class="navbar-brand" href="index.php">Schleswig-Holstein</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <img class="icon navbar-toggler-open" src="assets/img/icons/interface/menu.svg"
                        alt="menu interface icon" data-inject-svg />
                    <img class="icon navbar-toggler-close" src="assets/img/icons/interface/cross.svg"
                        alt="cross interface icon" data-inject-svg />
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <div class="py-2 py-lg-0">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a href="hilfe.php" class="nav-link dropdown-toggle" data-toggle="dropdown-grid"
                                    aria-expanded="false" aria-haspopup="true">Hilfe</a>
                            <li class="nav-item dropdown">
                                <a href="impressum.php" class="nav-link dropdown-toggle" data-toggle="dropdown-grid"
                                    aria-expanded="false" aria-haspopup="true">Impressum</a>
                            <li class="nav-item dropdown">
                            <li class="nav-item dropdown">
                                <a href="login-system/login.php" class="nav-link dropdown-toggle"
                                    data-toggle="dropdown-grid" aria-expanded="false" aria-haspopup="true">Login</a>
                            <li class="nav-item dropdown">
                                <a href="antrag.php" class="btn btn-primary ml-lg-3">Antrag stellen</a>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <section class="bg-primary-3 text-light text-center has-divider header-desktop-app">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8 col-md-9">
                    <h1 class="display-3">Melderegister-Sperre des Landes Schleswig-Holstein</h1>

                    <div class="d-flex flex-column flex-sm-row justify-content-center mt-4">


                    </div>
                </div>
            </div>
            <div class="row justify-content-center mt-6" data-aos="fade-up" data-delay="100">
                <div class="col-lg-10">
                    <img src="assets/img/schleswig-holstein3.jpg" alt="Image" class="rounded shadow-lg">
                </div>
            </div>
        </div>
        <div class="divider">
            <img src="assets/img/dividers/divider-2.svg" alt="graphical divider" data-inject-svg />
        </div>
    </section>


    <section>
        <div class="container">

            <div class="col">
                <h2 class="display-4">Übermittlungssperre</h2>
                <p>Mit einer Übermittlungssperre können Sie erreichen, dass Ihre Wohnortgemeinde nur noch in eng
                    begrenzten Ausnahmefällen eine Melderegisterauskunft (zum Beispiel Name, Anschrift) zu Ihren Daten
                    erteilt.</p>
                <p><span class="sbw-langtext"><strong>Tipp:</strong> Sie möchten nur die Weitergabe Ihrer Daten
                        verhindern an:</span></p>
                <ul class="sbw-langtext">
                    <li class="sbw-langtext"><span class="sbw-langtext">Adressbuchverlage</span></li>
                    <li class="sbw-langtext"><span class="sbw-langtext">Presse, Rundfunk und Mandatsträger zur
                            Veröffentlichung von Alters- und Ehejubiläen</span></li>
                    <li class="sbw-langtext"><span class="sbw-langtext">Parteien, Wählergruppen und anderen Trägern von
                            Wahlvorschlägen bei Wahlen und Abstimmungen</span></li>
                    <li class="sbw-langtext"><span class="sbw-langtext">die Bundeswehr</span></li>
                </ul>
                <p>Diese Sperren werden in der Regel ohne größere Nachfragen oder Gründe bewilligt.</p>

                <h2 class="display-4">Auskunftssperre</h2>
                <p>Die Auskunftssperre gemäß § 51 Abs. 1 Bundesmeldegesetz wird eingerichtet, wenn eine Gefahr für Leib
                    und Leben der Person besteht. Sie bewirkt, dass jegliche Weitergabe der Meldedaten an
                    nicht-öffentliche Stellen (z. B. alle privaten Stellen, aber auch ausländische Behörden)
                    unterbleibt, sofern nach Anhörung der betroffenen Person eine Gefahr nicht ausgeschlossen werden
                    kann. Voraussetzung für die Eintragung dieser Auskunftssperre ist eine konkrete Gefahr für Leib und
                    Leben des Betroffenen. Dazu reicht es beispielsweise nicht aus, dass eine Person einen riskanten
                    Beruf ausübt oder sich von den Nachbarn beobachtet fühlt. Vielmehr muss es konkrete Hinweise auf
                    eine Gefahr geben, wie z. B. ausgesprochene Drohungen oder sogar bereits erlebte
                    Handgreiflichkeiten. Des Weiteren bedarf es eines begründeten Antrages sowie geeigneter
                    Beweismittel. Der Antrag ist an keine Form gebunden, und eine kurze Begründung ist ausreichend.
                    Geeignete Beweismittel sind z. B. ärztliche Atteste, Gerichtsurteile oder Zeugenaussagen.</p>

                <h2 class="display-4">Unterlagen</h2>
                <p>Zu den benötigten Unterlagen gehören:</p>
                <li> Personalausweis</li>
                <li> richterliche Beschlüsse o.ä. </li>
                <p>Eine Notwendigkeit der jeweiligen Unterlagen richtet sich nach dem Einzelfall, daher ist keine
                    pauschale Aussage
                    darüber zu treffen, welche Unterlagen notwendig sind. Sollten gewisse Unterlagen eine Notwendigkeit
                    darstellen, so wird
                    sich die Behörde mit Ihnen postalisch in Verbindung setzten.</p>

                <h2 class="display-4">Kosten</h2>
                <p>Kosten fallen hierbei nicht an.</p>

                <h2 class="display-4">Rechtliche Grundlage</h2>
                <div class="sp-m-textContent-block-text">
                    <ul>
                        <li><a href="http://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=BMG+%C2%A7+50&amp;psml=bsbawueprod.psml&amp;max=true"
                                target="_blank" class="sp-m-externalLink">§ 50 Abs. 5 Bundesmeldegesetz (BMG)
                                (Melderegisterauskünfte in besonderen Fällen)</a></li>
                        <li><a href="http://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=BMG+%C2%A7+51&amp;psml=bsbawueprod.psml&amp;max=true"
                                target="_blank" class="sp-m-externalLink">§ 51 Bundesmeldegesetz (BMG)
                                (Auskunftssperren)</a></li>
                        <li><a href="http://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=BMG+%C2%A7+36&amp;psml=bsbawueprod.psml&amp;max=true"
                                target="_blank" class="sp-m-externalLink">§ 36 Abs. 2 Bundesmeldegesetz (BMG)
                                (Regelmäßige Datenübermittlungen)</a></li>
                        <li><a href="http://www.landesrecht-bw.de/jportal/?quelle=jlink&amp;query=SG+%C2%A7+58c&amp;psml=bsbawueprod.psml&amp;max=true"
                                target="_blank" class="sp-m-externalLink">§ 58 c Abs. 1 Satz 1 Soldatengesetz (SG)
                                (Übermittlung personenbezogener Daten durch die Meldebehörden)</a></li>
                    </ul>
                </div>
            </div>


        </div>


    </section>

    <!-- /.container -->


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


    <footer class="pb-4 bg-primary-3 text-light" id="footer">
        <div class="container">
            <div class="row mb-5">
                <div class="col">

                </div>
            </div>
            <div class="row mb-5">
                <div class="col-6 col-lg-3 col-xl-2">
                    <h5>Navigation</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a href="hilfe.php" class="nav-link">Hilfe</a>
                        </li>
                        <li class="nav-item">
                            <a href="impressum.php" class="nav-link">Impressum</a>
                        </li>
                        <li class="nav-item">
                            <a href="login-system/login.php" class="nav-link">Login</a>
                        </li>
                    </ul>
                </div>
                <div class="col-6 col-lg">
                    <h5>Kontakt</h5>
                    <ul class="list-unstyled">
                        <li class="mb-3 d-flex">
                            <img class="icon" src="assets/img/icons/theme/map/marker-1.svg" alt="marker-1 icon"
                                data-inject-svg />
                            <div class="ml-3">
                                <span>Musterstraße 1
                                    <br />Wildau, Brandenburg</span>
                            </div>
                        </li>
                        <li class="mb-3 d-flex">
                            <img class="icon" src="assets/img/icons/theme/communication/call-1.svg" alt="call-1 icon"
                                data-inject-svg />
                            <div class="ml-3">
                                <span>+49 1234 56789</span>
                                <span class="d-block text-muted text-small">Mo - Fr 9:00 - 17:00</span>
                            </div>
                        </li>
                        <li class="mb-3 d-flex">
                            <img class="icon" src="assets/img/icons/theme/communication/mail.svg" alt="mail icon"
                                data-inject-svg />
                            <div class="ml-3">
                                <a href="#">hello@verwaltung.de</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-5 col-xl-4 mt-3 mt-lg-0">

                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col col-md-auto text-center">
                    <small class="text-muted">&copy;2019 - Christoph Wolf, Jannik Walter, Ole Schwarz, Alexander
                        Schröder<a href="https://www.google.com/policies/privacy/"> Privacy Policy</a> und <a
                            href="https://policies.google.com/terms">Terms of Service.</a>
                    </small>
                </div>
            </div>
        </div>
    </footer>
    <a href="#" class="btn back-to-top btn-primary btn-round" data-smooth-scroll data-aos="fade-up"
        data-aos-offset="2000" data-aos-mirror="true" data-aos-once="false">
        <img class="icon" src="assets/img/icons/theme/navigation/arrow-up.svg" alt="arrow-up icon" data-inject-svg />
    </a>
    <!-- Required vendor scripts (Do not remove) -->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.js"></script>

    <!-- Optional Vendor Scripts (Remove the plugin script here and comment initializer script out of index.js if site does not use that feature) -->

    <!-- AOS (Animate On Scroll - animates elements into view while scrolling down) -->
    <script type="text/javascript" src="assets/js/aos.js"></script>
    <!-- Clipboard (copies content from browser into OS clipboard) -->
    <script type="text/javascript" src="assets/js/clipboard.js"></script>
    <!-- Fancybox (handles image and video lightbox and galleries) -->
    <script type="text/javascript" src="assets/js/jquery.fancybox.min.js"></script>
    <!-- Flatpickr (calendar/date/time picker UI) -->
    <script type="text/javascript" src="assets/js/flatpickr.min.js"></script>
    <!-- Flickity (handles touch enabled carousels and sliders) -->
    <script type="text/javascript" src="assets/js/flickity.pkgd.min.js"></script>
    <!-- Ion rangeSlider (flexible and pretty range slider elements) -->
    <script type="text/javascript" src="assets/js/ion.rangeSlider.min.js"></script>
    <!-- Isotope (masonry layouts and filtering) -->
    <script type="text/javascript" src="assets/js/isotope.pkgd.min.js"></script>
    <!-- jarallax (parallax effect and video backgrounds) -->
    <script type="text/javascript" src="assets/js/jarallax.min.js"></script>
    <script type="text/javascript" src="assets/js/jarallax-video.min.js"></script>
    <script type="text/javascript" src="assets/js/jarallax-element.min.js"></script>
    <!-- jQuery Countdown (displays countdown text to a specified date) -->
    <script type="text/javascript" src="assets/js/jquery.countdown.min.js"></script>
    <!-- jQuery smartWizard facilitates steppable wizard content -->
    <script type="text/javascript" src="assets/js/jquery.smartWizard.min.js"></script>
    <!-- Plyr (unified player for Video, Audio, Vimeo and Youtube) -->
    <script type="text/javascript" src="assets/js/plyr.polyfilled.min.js"></script>
    <!-- Prism (displays formatted code boxes) -->
    <script type="text/javascript" src="assets/js/prism.js"></script>
    <!-- ScrollMonitor (manages events for elements scrolling in and out of view) -->
    <script type="text/javascript" src="assets/js/scrollMonitor.js"></script>
    <!-- Smooth scroll (animation to links in-page)-->
    <script type="text/javascript" src="assets/js/smooth-scroll.polyfills.min.js"></script>
    <!-- SVGInjector (replaces img tags with SVG code to allow easy inclusion of SVGs with the benefit of inheriting colors and styles)-->
    <script type="text/javascript" src="assets/js/svg-injector.umd.production.js"></script>
    <!-- TwitterFetcher (displays a feed of tweets from a specified account)-->
    <script type="text/javascript" src="assets/js/twitterFetcher_min.js"></script>
    <!-- Typed text (animated typing effect)-->
    <script type="text/javascript" src="assets/js/typed.min.js"></script>
    <!-- Required theme scripts (Do not remove) -->
    <script type="text/javascript" src="assets/js/theme.js"></script>
    <!-- Removes page load animation when window is finished loading -->
    <script type="text/javascript">
        window.addEventListener("load", function () { document.querySelector('body').classList.add('loaded'); });
    </script>

</body>

</html>