Herzlich Willkommen bei der Melderegistersperre des Landes Schleswig-Holstein, 
im Rahmen des Studienfaches eGovernment II im Studiengang VIBB-18.
-------------------------------------------------------------------------------
Die folgende Anleitung bezieht sich auf Windows-Betriebssysteme. Für andere Betriebssysteme ist der Vorgang der gleiche, jedoch können Pfade etc. anders sein. 

Wir haben uns entschieden, eine Stand-Alone-Anwendung erstellen. Unsere Webanwendung läuft ohne weitere Bestandteile wie ein Portal oder ähnliches.

Die Installation und Einrichtung dauert ca. 3 Minuten (exklusive Download-Zeiten)
Um dieses Projekt lauffähig zu machen, hier eine Quickstart Anleitung:

1. Öffnen Sie die Konsole in einem beliebigen Zielordner z.B. Desktop/Melderegistersperre_SH

2. Clonen Sie unser Repository mit:
"git clone https://gitlab.com/pittiplatsch1/ozg_sperre.git"

3. Installieren sie XAMPP. Wir empfehlen XAMPP unter 
folgendem Link herunterzuladen und dem Installer zu folgen. Wir empfehlen XAMPP 
unter C:/XAMPP zu installieren.
https://www.apachefriends.org/de/download.html

4. Nach Installation wechseln Sie in den Installationsorder von XAMPP (C:/XAMPP).
Den Ordner "Melderegistersperre" aus unserem Git-Repository fügen Sie unter 
XAMPP/htdocs ein. 

5. Fügen Sie den mysql-Ordner aus unserem Git-Repository in das XAMPP-Hauptverzeichnis (C:/XAMPP) ein. Dieser ist bereits vorhanden, überschreiben Sie diesen daher.

6. Anschließend starten Sie die xampp-control.exe. Diese befindet sich um Hauptverzeichnis von XAMPP (z.B. C:/XAMPP)
In dieser Haben Sie die Möglichkeit die Anwendungen zu 
starten. Für die Verwendung dieses Projektes ist die Nutzung vom Apache Webserver 
und MySQL nötig. Beides muss gestartet werden. 

!!!WICHTIG!!!
Sollte MySQL nicht starten (rote Fehlermeldung in xampp-control.exe) 
wechseln Sie in den Ordner XAMPP/mysql/bin und starten dort die Konsole und geben 
Folgenden Befehl ein: "mysqld --skip-grant-table"
MySQL startet nun fehlerfrei.
Sollte MySQL Ordnungsgemäß über xampp-control.exe gestartet sein, brauchen Sie 
dies nicht durchzuführen. 
!!!WICHTIG!!!

7. Sind alle Anwendungen gestartet, erreichen Sie die Webseite unter der Adresse
"localhost/melderegistersperre" durch Ihren Browser


